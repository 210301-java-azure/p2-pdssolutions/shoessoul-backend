package dev.pdssolutions.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Component
@Table(name = "shoes")
public class Shoe implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String brand;
    private String style;
    private String name;
    private double price;


    @JoinColumn(name = "id")
    private Integer departmentId;
    private String description;
    private String photoUrl;


    public Shoe() {
        super();
    }

    public Shoe(int id, String brand, String name, double price, String style, Integer departmentId, String description, String photoUrl) {
        this.id = id;
        this.brand = brand;
        this.name = name;
        this.price = price;
        this.style = style;
        this.departmentId = departmentId;
        this.description = description;
        this.photoUrl = photoUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartment(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shoe shoe = (Shoe) o;
        return id == shoe.id && Double.compare(shoe.price, price) == 0 && brand.equals(shoe.brand) && name.equals(shoe.name) && description.equals(shoe.description) && departmentId.equals(shoe.departmentId) && style.equals(shoe.style) && photoUrl.equals(shoe.photoUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brand, name, price, style, departmentId, description, photoUrl);
    }

    @Override
    public String toString() {
        return "Shoe{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", price=" + style +
                ", department=" + departmentId +
                ", description='" + description + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                '}';
    }
}