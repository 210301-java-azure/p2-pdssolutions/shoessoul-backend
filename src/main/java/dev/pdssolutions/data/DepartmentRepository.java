package dev.pdssolutions.data;

import dev.pdssolutions.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {

    public Department getDepartmentByName(String name);

    public Department getDepartmentById(int id);
}
