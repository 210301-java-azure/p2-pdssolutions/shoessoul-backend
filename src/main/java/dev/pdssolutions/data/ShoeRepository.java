package dev.pdssolutions.data;

import dev.pdssolutions.entity.Shoe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShoeRepository extends JpaRepository<Shoe, Integer> {

    public Shoe getShoeByName(String name);

    public List<Shoe> getShoesByBrand(String brand);

    public List<Shoe> getShoesByDepartmentId(int id);
}
