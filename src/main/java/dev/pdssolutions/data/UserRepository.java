package dev.pdssolutions.data;

import dev.pdssolutions.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

//    public List<User> findUsersByNameContaining(String someVariable);
    public User findUserByUsernameAndPassword(String username, String password);
    public User findUserByUsername(String username);
}
