package dev.pdssolutions.aspects;

import dev.pdssolutions.exceptions.UnauthorizedException;
import dev.pdssolutions.service.UserService;
import dev.pdssolutions.util.JwtUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect
public class AuthAspect {

    private Logger logger = LogManager.getLogger(AuthAspect.class);

    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtil jwtTokenUtil;

    @Pointcut("within(dev.pdssolutions.controller..*) " +
            "&& !within(dev.pdssolutions.controller.AuthController) " +
            "&& !execution(* dev.pdssolutions.controller.UserController.registerUser(..)) " +
            "&& !within(dev.pdssolutions.controller.ZipCodeApiController)")
    public void authorizePointcut() {}

    @Around("authorizePointcut()")
    public ResponseEntity authorizeRequest(ProceedingJoinPoint jp) throws Throwable {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization");
        if(token!=null){
            String username = jwtTokenUtil.extractUsername(token);

            if (jwtTokenUtil.validateToken(token, userService.findUserByUsername(username))) {
                logger.info("validate token successful");
                return (ResponseEntity) jp.proceed();
                //Assign role rules with access manager
            }
            else {
                logger.warn("improper authorization");
                throw new UnauthorizedException("token was incorrect");
            }
        } else {
            logger.warn("auth header not present");
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }

}
