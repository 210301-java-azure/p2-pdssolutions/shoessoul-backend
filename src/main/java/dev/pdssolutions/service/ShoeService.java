package dev.pdssolutions.service;

import dev.pdssolutions.data.ShoeRepository;
import dev.pdssolutions.entity.Shoe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoeService {

    @Autowired
    ShoeRepository shoeRepo;

    public List<Shoe> getAll() {
        return shoeRepo.findAll();
    }

    public Shoe getById(int id) {
        return shoeRepo.getOne(id);
    }

    public Shoe createShoe(Shoe shoe) {
        return shoeRepo.save(shoe);
    }

    public void deleteShoe(int id) {
        shoeRepo.deleteById(id);
    }

    public Shoe getShoeByName(String name) {
        return shoeRepo.getShoeByName(name);
    }

    public List<Shoe> getShoesByBrand(String brand) {
        return shoeRepo.getShoesByBrand(brand);
    }

    public Shoe updateShoe(Shoe shoe, int id) {
        if (shoe.getId() != id) {
            throw new IllegalArgumentException("No such ID exists, please enter a valid ID.");

        }
        return shoeRepo.save(shoe);
    }

    public List<Shoe> getShoesByDepartmentId(int id) {
        return shoeRepo.getShoesByDepartmentId(id);
    }
}
