package dev.pdssolutions.service;

import dev.pdssolutions.controller.ZipCodeApiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;


@Service
public class ZipCodeApiService {
    TestRestTemplate restTemplate = new TestRestTemplate();

    public Object getDistanceInMiles(Integer zipcode) {
        HttpEntity<Object> request = new HttpEntity<>(zipcode);
        ResponseEntity<Object> responseEntity = this.restTemplate.exchange("https://www.zipcodeapi.com/rest/yB24fYr6u0BDA6Ndu6sVTIwI0IsLf5Efj5YMBcULDFx2D5PLWgCaow6k7Wk2jpbz/distance.json/78754/" + zipcode + "/mile",
                HttpMethod.GET,request,
                Object.class);

        return responseEntity.getBody();
    }
}
