package dev.pdssolutions.service;

import dev.pdssolutions.entity.User;
import dev.pdssolutions.data.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepo;


    public User authenticate(String username , String password){
        return userRepo.findUserByUsernameAndPassword(username, password);
    }

    public List<User> getAll(){
        return userRepo.findAll();
    }

    public User getById(int id){
        return userRepo.getOne(id);
    }

    public User register(User user){
        return userRepo.save(user);
    }

    public User findUserByUsername(String name){
        return userRepo.findUserByUsername(name);
    }

}
