package dev.pdssolutions.service;

import dev.pdssolutions.data.DepartmentRepository;
import dev.pdssolutions.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    DepartmentRepository departmentRepo;

    public List<Department> getAll() {
        return departmentRepo.findAll();
    }

    public Department getById(int id) {
        return departmentRepo.getDepartmentById(id);
    }

    public Department createDepartment(Department department) {
        return departmentRepo.save(department);
    }

    public void deleteDepartment(int id) {
        departmentRepo.deleteById(id);
    }

    public Department getDepartmentByName(String name) {
        return departmentRepo.getDepartmentByName(name);
    }

    public Department updateDepartment(Department department, int id) {
        if (department.getId() != id) {
            throw new IllegalArgumentException("No such ID exists, please enter a valid ID.");

        }
        return departmentRepo.save(department);
    }
}
