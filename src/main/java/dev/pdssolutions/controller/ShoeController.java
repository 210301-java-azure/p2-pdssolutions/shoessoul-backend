package dev.pdssolutions.controller;

import dev.pdssolutions.entity.Shoe;
import dev.pdssolutions.service.ShoeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shoes")
@CrossOrigin
public class ShoeController {

    private static Logger logger = LogManager.getLogger(ShoeController.class);

    @Autowired
    private ShoeService shoeService;

    @GetMapping
    public ResponseEntity<List<Shoe>> getAll() {
        logger.info("Getting master list of all shoes");
        return ResponseEntity.ok().body(shoeService.getAll());
    }


    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Shoe> getById(@PathVariable("id") int id) {
        logger.info("Getting shoe with id: {}", id);
        return ResponseEntity.ok().body(shoeService.getById(id));
    }


    @PostMapping(consumes = "application/json")
    public ResponseEntity<Shoe> createNewShoe(@RequestBody Shoe shoe) {
        logger.info("Creating new shoe");
        return ResponseEntity.ok().body(shoeService.createShoe(shoe));
    }


    @DeleteMapping(value = "/{id}")
    public void deleteShoe(@PathVariable int id) {
        logger.info("Successfully deleting shoe");
        shoeService.deleteShoe(id);
    }


    @GetMapping(value = "/name/{name}", produces = "application/json")
    public ResponseEntity<Shoe> getShoeByName(@PathVariable String name) {
        logger.info("Getting shoe by name {}", name);
        return ResponseEntity.ok().body(shoeService.getShoeByName(name));
    }


    @GetMapping(value = "/brand/{brand}", produces = "application/json")
    public ResponseEntity<List<Shoe>> getShoesByBrand(@PathVariable String brand) {
        logger.info("Getting shoe by brand {}", brand);
        return ResponseEntity.ok().body(shoeService.getShoesByBrand(brand));
    }

    @PutMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Shoe> updateShoe(@RequestBody Shoe shoe, @PathVariable int id) {
        logger.info("Successfully updating shoe with id: {}", id);
        return ResponseEntity.ok().body(shoeService.updateShoe(shoe, id));
    }

    @GetMapping(value = "/departments/{id}", produces = "application/json")
    public ResponseEntity<List<Shoe>> getShoesByDepartmentId(@PathVariable int id) {
        logger.info("Getting Shoes with Department ID: {}", id);
        return ResponseEntity.ok().body(shoeService.getShoesByDepartmentId(id));
    }
}
