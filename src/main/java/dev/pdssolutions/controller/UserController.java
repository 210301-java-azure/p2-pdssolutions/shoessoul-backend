package dev.pdssolutions.controller;

import dev.pdssolutions.entity.Shoe;
import dev.pdssolutions.entity.User;
import dev.pdssolutions.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController {

    private static Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<User>> getAll() {
        logger.info("Getting master list of all users");
        return ResponseEntity.ok().body(userService.getAll());
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<User> getById(@PathVariable("id") int id) {
        logger.info("Getting user with id: {}", id);
        return ResponseEntity.ok().body(userService.getById(id));
    }

    @GetMapping(value = "/username/{username}", produces = "application/json")
    public ResponseEntity<User> getUserByUsername(@PathVariable String username) {
        logger.info("Getting user by username {}", username);
        return ResponseEntity.ok().body(userService.findUserByUsername(username));
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<User> registerUser(@RequestBody User user) throws IOException {

        User newUser = new User(user.getUsername(), user.getPassword(), user.getRole());
        System.out.println(newUser.toString());
        userService.register (newUser);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }
}
