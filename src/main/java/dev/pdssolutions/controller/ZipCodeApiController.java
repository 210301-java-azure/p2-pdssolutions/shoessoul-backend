package dev.pdssolutions.controller;

import dev.pdssolutions.service.ZipCodeApiService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class ZipCodeApiController {

    private static Logger logger = LogManager.getLogger(ZipCodeApiController.class);

    @Autowired
    private ZipCodeApiService zipCodeApiService;

    @RequestMapping(value = "/distance", method = RequestMethod.GET)
    public ResponseEntity<Object> getDistanceInMiles(@RequestParam (value = "zipcode", required = false) Integer zipcode) {
        logger.info("Getting distance to zipcode: {}", zipcode);
        return ResponseEntity.ok().body(zipCodeApiService.getDistanceInMiles(zipcode));
    }
}
