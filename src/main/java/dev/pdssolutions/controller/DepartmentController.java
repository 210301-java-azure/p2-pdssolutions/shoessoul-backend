package dev.pdssolutions.controller;

import dev.pdssolutions.entity.Department;
import dev.pdssolutions.service.DepartmentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.parser.Part;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/departments")
@CrossOrigin
public class DepartmentController {

    private static Logger logger = LogManager.getLogger(DepartmentController.class);

    @Autowired
    private DepartmentService departmentService;

    @GetMapping
    public ResponseEntity<List<Department>> getAll() {
        logger.info("Getting master list of all Departments");
        return ResponseEntity.ok().body(departmentService.getAll());
    }


    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Department> getById(@PathVariable("id") int id) {
        logger.info("Getting Department with ID: {}", id);
        return ResponseEntity.ok().body(departmentService.getById(id));
    }


    @PostMapping(consumes = "application/json")
    public ResponseEntity<Department> createNewDepartment(@RequestBody Department department) {
        logger.info("Creating new department");
        return ResponseEntity.ok().body(departmentService.createDepartment(department));
    }


    @DeleteMapping(value = "/{id}")
    public void deleteDepartment(@PathVariable int id) {
        logger.info("Successfully deleting department");
        departmentService.deleteDepartment(id);
    }


    @GetMapping(value = "/name/{name}", produces = "application/json")
    public ResponseEntity<Department> getDepartmentByName(@PathVariable String name) {
        logger.info("Getting Department by name {}", name);
        return ResponseEntity.ok().body(departmentService.getDepartmentByName(name));
    }


    @PutMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Department> updateDepartment(@RequestBody Department department, @PathVariable int id) {
        logger.info("Successfully updating Department with ID: {}", id);
        return ResponseEntity.ok().body(departmentService.updateDepartment(department, id));
    }



}
