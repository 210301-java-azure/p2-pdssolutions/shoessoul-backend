package dev.pdssolutions.controller;

import dev.pdssolutions.entity.User;
import dev.pdssolutions.exceptions.UnauthorizedException;
import dev.pdssolutions.service.UserService;
import dev.pdssolutions.util.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtil jwtTokenUtil;

    //    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestParam("username")String username,
                                        @RequestParam("password")String password){

        User userDetails = userService.authenticate(username, password);

        if(userDetails!=null) {

            String token = jwtTokenUtil.generateToken(userDetails);
            logger.info(token);
            return ResponseEntity.ok()
                    .header("Authorization", token)
                    .header("Access-Control-Expose-Headers", "Authorization")
                    .body("{\"message\": \"success!\"}");
        }
        throw new UnauthorizedException("Incorrect credentials");
    }
}
