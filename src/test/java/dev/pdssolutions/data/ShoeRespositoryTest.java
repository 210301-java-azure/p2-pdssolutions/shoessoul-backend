package dev.pdssolutions.data;

import dev.pdssolutions.entity.Shoe;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
class ShoeRespositoryTest {

    @Autowired
    ShoeRepository shoeRepo;

    @Test
    @Transactional
    public void AddNewShoe() {

        shoeRepo.deleteAll();

        Shoe shoe1 = new Shoe(1, "brand1", "name1", 150.00, "style1", 1, "saogh aoisg aoiguoi aoig", "");
        Shoe shoe2 = new Shoe(2, "brand2", "name2", 2000.00, "style2", 2, "adfgi aoidfug aig", "");

        shoeRepo.save(shoe1);
        shoeRepo.save(shoe2);


        Assertions.assertEquals(shoe1.getId(), 1);
        Assertions.assertEquals(shoe2.getName(), "name2");
    }

}
