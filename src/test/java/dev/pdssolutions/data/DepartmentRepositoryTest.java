package dev.pdssolutions.data;

import dev.pdssolutions.entity.Department;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
class DepartmentRepositoryTest {

        @Autowired
        DepartmentRepository DepartmentRepo;

        @Test
        @Transactional
        public void AddNewDepartment() {

            DepartmentRepo.deleteAll();

            Department Department1 = new Department(1000, "Something");
            Department Department2 = new Department(1001, "SomethingElse");

            DepartmentRepo.save(Department1);
            DepartmentRepo.save(Department2);


            Assertions.assertEquals(Department1.getId(), 1000);
            Assertions.assertEquals(Department2.getName(), "SomethingElse");
        }

}
