package dev.pdssolutions.controller;

import dev.pdssolutions.entity.Shoe;
import dev.pdssolutions.service.ShoeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ShoeControllerTest {

    private MockMvc mockMvc;

    @MockBean
    ShoeService shoeService;

    @InjectMocks
    ShoeController shoeController;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(shoeController).build();
    }

    @Test
    public void testGetAllShoes() throws Exception {

        List<Shoe> shoeList = new ArrayList<>();

        shoeList.add(new Shoe(1, "Atlantic Mannagrass", "Inverse", 1060.50, "Business-focused", 1, "donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus", "http://dummyimage.com/126x100.png/5fa2dd/ffffff"));
        shoeList.add(new Shoe(2, "Fluoride", "Buell Park Phacelia", 263.88, "vestibulum", 5, "velit id pretium iaculis diam erat fermentum justo", "http://dummyimage.com/197x100.png/cc0000/ffffff"));
        shoeList.add(new Shoe(3, "Cialis", "Island False Bindweed", 898.12, "quis", 3, "orci nullam molestie nibh in lectus pellentesque at", "http://dummyimage.com/193x100.png/ff4444/ffffff"));
        shoeList.add(new Shoe(4, "Amoxicillin", "Pennsylvania Dung Moss", 430.57, "consequat", 1, "lectus in est risus", "http://dummyimage.com/133x100.png/ff4444/ffffff"));
        shoeList.add(new Shoe(5, "Eve", "Southern Annual Saltmarsh Aster", 743.01, "risus", 1, "semper porta volutpat", "http://dummyimage.com/232x100.png/dddddd/000000"));
        shoeList.add(new Shoe(6, "Cardizem", "lorem", 994.65, "ipsum", 3, "dolor sit amet", "http://dummyimage.com/224x100.png/5fa2dd/ffffff"));

        when(shoeService.getAll()).thenReturn(shoeList);

        this.mockMvc.perform(get("/shoes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].brand")
                        .value(hasItems("Cardizem", "Eve", "Cialis")));
    }

    @Test
    public void testGetShoeByName() throws Exception {
       Shoe shoe1 = new Shoe(5, "Cardizem", "lorem", 994.65, "ipsum", 3, "dolor sit amet", "http://dummyimage.com/224x100.png/5fa2dd/ffffff");

        when(shoeService.getShoeByName("lorem")).thenReturn(shoe1);

        this.mockMvc.perform(get("/shoes/name/lorem"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$..id", hasItems(shoe1.getId())));
    }

    @Test
    public void testGetShoeById() throws Exception {
        Shoe shoe1 = new Shoe(5, "Cardizem", "lorem", 994.65, "ipsum", 3, "dolor sit amet", "http://dummyimage.com/224x100.png/5fa2dd/ffffff");

        when(shoeService.getById(5)).thenReturn(shoe1);

        this.mockMvc.perform(get("/shoes/5"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$..name", hasItems(shoe1.getName())));
    }

    @Test
    public void rootContext_ShouldRespondWith404() throws Exception {
        mockMvc.perform(get("/")).andExpect(status().isNotFound());
    }
}
