package dev.pdssolutions.controller;

import dev.pdssolutions.entity.Department;
import dev.pdssolutions.service.DepartmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class DepartmentControllerTest {
    private MockMvc mockMvc;

    @MockBean
    DepartmentService departmentService;

    @InjectMocks
    DepartmentController departmentController;

    @BeforeEach
    public void setup() { this.mockMvc = MockMvcBuilders.standaloneSetup(departmentController).build(); }

    @Test
    public void testGetAllDepartments() throws Exception {
        List<Department> departmentList = new ArrayList<>();

        departmentList.add(new Department(1, "Women"));
        departmentList.add(new Department(1, "Men"));
        departmentList.add(new Department(1, "Children"));

        when(departmentService.getAll()).thenReturn(departmentList);

        this.mockMvc.perform(get("/departments"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("[*].name")
                    .value(hasItems("Women", "Men", "Children")));
    }

    @Test
    public void testGetDepartmentByName() throws Exception {
        Department department1 = new Department(2, "Men");

        when(departmentService.getDepartmentByName("Men")).thenReturn(department1);

        this.mockMvc.perform(get("/departments/name/Men"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$..id", hasItems(department1.getId())));
    }

    @Test
    public void testGetDepartmentById() throws Exception {
        Department department1 = new Department(2, "Men");

        when(departmentService.getById(2)).thenReturn(department1);

        this.mockMvc.perform(get("/departments/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$..name", hasItems(department1.getName())));
    }
}
