package dev.pdssolutions.controller;

import dev.pdssolutions.entity.Department;
import dev.pdssolutions.entity.Shoe;
import dev.pdssolutions.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import dev.pdssolutions.util.JwtUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class UserControllerIntegrationTest {
    
    @Autowired
    private TestRestTemplate restTemplate;

    String token;
    HttpHeaders headers;

    @BeforeEach
    public void login() {
        JwtUtil jwtUtil = new JwtUtil();
        User userDetails = new User(1, "userAdmin", "Admin", "pass123");
        token = jwtUtil.generateToken(userDetails);
        headers = new HttpHeaders();
        headers.add("Authorization", token);
    }

    @Test
    public void testGetAllUsers() {

        HttpEntity<User[]> request = new HttpEntity<>(headers);
        ResponseEntity<User[]> responseEntity = this.restTemplate.exchange("http://52.149.215.97/users",
                HttpMethod.GET, request,
                User[].class);
        HttpStatus status = responseEntity.getStatusCode();
        User[] Users = responseEntity.getBody();
        HttpStatus expectedStatus = HttpStatus.OK;
        assertEquals(expectedStatus, status);
        assertTrue(Users.length > 0);
    }

    @Test
    public void testGetOneById() {

        HttpEntity<User> request = new HttpEntity<>(headers);
        ResponseEntity<User> responseEntity = this.restTemplate.exchange("http://52.149.215.97/users/1",
                HttpMethod.GET, request,
                User.class);
        User actual = responseEntity.getBody();
        User expected = new User(1, "userAdmin", "pass123","ADMIN");

        assertEquals(expected,actual);
    }

    @Test
    public void testGetOneByUsername() {

        HttpEntity<User> request = new HttpEntity<>(headers);
        ResponseEntity<User> responseEntity = this.restTemplate.exchange("http://52.149.215.97/users/username/userAdmin",
                HttpMethod.GET, request,
                User.class);
        User actual = responseEntity.getBody();
        User expected = new User(1, "userAdmin", "pass123","ADMIN");

        assertEquals(expected,actual);
    }
}
