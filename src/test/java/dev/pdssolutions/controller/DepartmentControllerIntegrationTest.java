package dev.pdssolutions.controller;

import dev.pdssolutions.entity.Department;
import dev.pdssolutions.entity.Shoe;
import dev.pdssolutions.entity.User;
import dev.pdssolutions.util.JwtUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DepartmentControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    String token;
    HttpHeaders headers;

    @BeforeEach
    public void login() {
        JwtUtil jwtUtil = new JwtUtil();
        User userDetails = new User(1, "userAdmin", "Admin", "pass123");
        token = jwtUtil.generateToken(userDetails);
        headers = new HttpHeaders();
        headers.add("Authorization", token);
    }

    @Test
    public void testGetAllDepartments() {

        HttpEntity<Department[]> request = new HttpEntity<>(headers);
        ResponseEntity<Department[]> responseEntity = this.restTemplate.exchange("http://52.149.215.97/departments",
                HttpMethod.GET, request,
                Department[].class);
        HttpStatus status = responseEntity.getStatusCode();
        Department[] departments = responseEntity.getBody();
        HttpStatus expectedStatus = HttpStatus.OK;
        assertEquals(expectedStatus, status);
        assertTrue(departments.length > 0);
    }

    @Test
    public void testGetOneById() {

        HttpEntity<Department> request = new HttpEntity<>(headers);
        ResponseEntity<Department> responseEntity = this.restTemplate.exchange("http://52.149.215.97/departments/1",
                HttpMethod.GET, request,
                Department.class);
        Department actual = responseEntity.getBody();
        Department expected = new Department(1, "Men");

        assertEquals(expected,actual);
    }

    @Test
    public void testGetOneByName() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);
        HttpEntity<Department> request = new HttpEntity<>(headers);
        ResponseEntity<Department> responseEntity = this.restTemplate.exchange("http://52.149.215.97/departments/name/Men",
                HttpMethod.GET, request,
                Department.class);
        Department actual = responseEntity.getBody();
        Department expected = new Department(1, "Men");

        assertEquals(expected,actual);
    }
}
