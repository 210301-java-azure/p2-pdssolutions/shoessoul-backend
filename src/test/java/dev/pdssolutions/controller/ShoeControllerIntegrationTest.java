package dev.pdssolutions.controller;

import dev.pdssolutions.entity.Department;
import dev.pdssolutions.entity.Shoe;
import dev.pdssolutions.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import dev.pdssolutions.util.JwtUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ShoeControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    String token;
    HttpHeaders headers;

    @BeforeEach
    public void login() {
        JwtUtil jwtUtil = new JwtUtil();
        User userDetails = new User(1, "userAdmin", "Admin", "pass123");
        token = jwtUtil.generateToken(userDetails);
        headers = new HttpHeaders();
        headers.add("Authorization", token);
    }

    @Test
    public void testGetAllShoes() {

        HttpEntity<Shoe[]> request = new HttpEntity<>(headers);
        ResponseEntity<Shoe[]> responseEntity = this.restTemplate.exchange("http://52.149.215.97/shoes",
                HttpMethod.GET, request,
                Shoe[].class);
        HttpStatus status = responseEntity.getStatusCode();
        Shoe[] shoes = responseEntity.getBody();
        HttpStatus expectedStatus = HttpStatus.OK;
        assertEquals(expectedStatus, status);
        assertTrue(shoes.length > 0);
    }

    @Test
    public void testGetOneById() {

        HttpEntity<Shoe> request = new HttpEntity<>(headers);
        ResponseEntity<Shoe> responseEntity = this.restTemplate.exchange("http://52.149.215.97/shoes/10",
                HttpMethod.GET, request,
                Shoe.class);
        Shoe actual = responseEntity.getBody();
        Shoe expected = new Shoe(10, "Test", "Testing", 2000.5, "Testing-focused", 4,"donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus","http://dummyimage.com/126x100.png/5fa2dd/ffffff");

        assertEquals(expected,actual);
    }

    @Test
    public void testGetOneByName() {

        HttpEntity<Shoe> request = new HttpEntity<>(headers);
        ResponseEntity<Shoe> responseEntity = this.restTemplate.exchange("http://52.149.215.97/shoes/name/Testing",
                HttpMethod.GET, request,
                Shoe.class);
        Shoe actual = responseEntity.getBody();
        Shoe expected = new Shoe(10, "Test", "Testing", 2000.5, "Testing-focused", 4,"donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus","http://dummyimage.com/126x100.png/5fa2dd/ffffff");

        assertEquals(expected,actual);
    }
}
