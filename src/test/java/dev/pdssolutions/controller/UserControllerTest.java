package dev.pdssolutions.controller;

import dev.pdssolutions.entity.User;
import dev.pdssolutions.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

    private MockMvc mockMvc;

    @MockBean
    UserService UserService;

    @InjectMocks
    UserController UserController;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(UserController).build();
    }

    @Test
    public void testGetAllUsers() throws Exception {

        List<User> UserList = new ArrayList<>();

        UserList.add(new User(1,"userAdmin1", "pass123", "ADMIN"));
        UserList.add(new User(1,"userStandard1", "pass456", "STANDARD"));
        UserList.add(new User(1,"userStandard2", "pass789", "STANDARD"));

        when(UserService.getAll()).thenReturn(UserList);

        this.mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].username")
                        .value(hasItems("userAdmin1", "userStandard1", "userStandard2")));
    }

    @Test
    public void testGetUserByUsername() throws Exception {
        User user1 = new User(1,"userAdmin1", "pass123", "ADMIN");

        when(UserService.findUserByUsername("userAdmin1")).thenReturn(user1);

        this.mockMvc.perform(get("/users/username/userAdmin1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$..id", hasItems(user1.getId())));
    }

    @Test
    public void testGetUserById() throws Exception {
        User user1 = new User(1,"userAdmin1", "pass123", "ADMIN");

        when(UserService.getById(1)).thenReturn(user1);

        this.mockMvc.perform(get("/users/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$..username", hasItems(user1.getUsername())));
    }
}
