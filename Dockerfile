FROM java:8
COPY build/libs/shoessoul-backend-0.0.1-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar shoessoul-backend-0.0.1-SNAPSHOT.jar
