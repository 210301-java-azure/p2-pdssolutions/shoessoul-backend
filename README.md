# ShoeSoul 

## Project Description

ShoeSoul is an e-commerce shoe store web application designed for inventory management and shoe sales. The site provides detailed information about the products, enables visitors to fill a shopping cart, check shipping prices based on location, and make mock orders, all through an attractive graphical user interface.

## Technologies Used

* Java 8
* Angular
* Spring Boot (with Spring Web, Spring Web, Spring Data-JPA starters)
* JDBC
* Jackson Data-bind
* Gradle
* Tomcat
* MS SQL Server
* Docker
* Azure Linux VM
* Jenkins automation server
* Azure Web App Service
* BLK Design System for Angular
* JUnit
* Log4J

## Features

* Login
* Listing shoes in inventory
* Filling shopping cart with selected items
* Getting shipping prices, calculated from zipcodeapi.com API calls
* Checkout

## Contributors
* DesJohnna Gray
* Sebastian Perez
* Peter Slavin
